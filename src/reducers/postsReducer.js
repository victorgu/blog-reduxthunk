// to mantain a list of posts that we have fetched from the jsonPlaceholder
export default (state = [], action) => {
  switch (action.type) {
    case 'FETCH_POSTS':
      return action.payload
    default:
      return state
  }
}