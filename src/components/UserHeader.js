import React, { Component } from 'react'
import { connect } from 'react-redux'

class UserHeader extends Component {
  render() {
    // finding the Post author || get all the users!
    // const user = this.props.users.find(user => user.id === this.props.userId)
    
    // with the change in mapStateToProps now we have only one user in props.
    const { user } = this.props
    
    if (!user) {
      return null
    }
    return <div className="header">{user.name}</div>
  }
}

const mapStateToProps = (state, ownProps) => {
  return { user: state.users.find(user => user.id === ownProps.userId) }
}

export default connect(mapStateToProps)(UserHeader)